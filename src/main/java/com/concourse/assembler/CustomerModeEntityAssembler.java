/**
*
*
*   
*
* Author : Shaik Fareed
*
* 
*
**/ 
package com.concourse.assembler;

import java.util.Iterator;
import java.util.List;

import com.concourse.entity.Address;
import com.concourse.entity.Customer;
import com.concourse.model.CustomerModel;

public class CustomerModeEntityAssembler {

	/**
	 * @param customerModel
	 * @return
	 */
	public static Customer populateEntityFromModel(CustomerModel customerModel) {
		Customer customer = new Customer();
		customer.setId(customerModel.getId());
		customer.setAge(customerModel.getAge());
		customer.setFirstName(customerModel.getFirstName());
		customer.setLastName(customerModel.getLastName());
		customer.setListOfAddress(customerModel.getListOfAddress());
		List<Address> list = customerModel.getListOfAddress();
		
		Iterator<Address> iterator = list.iterator();
		
		
		while(iterator.hasNext()){
			Address address = iterator.next();
			
			if(address.getAddressLine()==null){
				iterator.remove();
			}
		}
		
		customer.setListOfAddress(list);
		
		return customer;
	}

	/**
	 * @param customer
	 * @return
	 */
	public static CustomerModel populateModelFromEntity(Customer customer) {
		CustomerModel customerModel = new CustomerModel();
		customerModel.setAge(customer.getAge());
		customerModel.setId(customer.getId());
		customerModel.setFirstName(customer.getFirstName());
		customerModel.setLastName(customer.getLastName());
		customerModel.setListOfAddress(customer.getListOfAddress());
		List<Address> list = customer.getListOfAddress();
		
		Iterator<Address> iterator = list.iterator();
		
		
		while(iterator.hasNext()){
			Address address = iterator.next();
			
			if(address.getAddressLine()==null){
				iterator.remove();
			}
		}
		
		customerModel.setListOfAddress(list);
		return customerModel;
	}

	
	
}
