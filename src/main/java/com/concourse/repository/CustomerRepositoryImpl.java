/**
 * 
 */
package com.concourse.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cinchapi.concourse.Concourse;
import com.cinchapi.concourse.thrift.Operator;
import com.cinchapi.concourse.util.PrettyLinkedHashMap;
import com.cinchapi.concourse.util.Random;
import com.concourse.entity.Customer;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import scala.collection.concurrent.Map;

/**
 * @author sfareed
 *
 */
@Repository
public class CustomerRepositoryImpl extends BaseRepository implements CustomerRepository {
	
	@Autowired
	Concourse concourse;
	
	private final static String CLASS_KEY_NAME = "_class";
	

	/* (non-Javadoc)
	 * @see com.concourse.repository.CustomerRepository#findByFirstName(java.lang.String)
	 */
	@Override
	public Customer findByFirstName(String firstName) {
		
		return (Customer) concourse.select("firstName = "+firstName);
	}

	/* (non-Javadoc)
	 * @see com.concourse.repository.CustomerRepository#findByFirstNameAndLastName(java.lang.String, java.lang.String)
	 */
	@Override
	public Customer findByFirstNameAndLastName(String firstName, String lastName) {
		PrettyLinkedHashMap map = (PrettyLinkedHashMap) concourse.select("firstName = "+firstName,"lastName = "+lastName);
		return null;
	}

	/* (non-Javadoc)
	 * @see com.concourse.repository.CustomerRepository#saveCustomer(com.concourse.entity.Customer)
	 */
	@Override
	public void saveCustomer(Customer customer) {
        try {
            Multimap<String, Object> data = HashMultimap.create();
            data.put(CLASS_KEY_NAME, Customer.class);
            data.put("firstName", customer.getFirstName());
            data.put("lastName", customer.getLastName());
            data.put("id", Random.getInt()+"");
            concourse.insert(data);
        }
        finally {
            release(concourse);
        }
	}

	/* (non-Javadoc)
	 * @see com.concourse.repository.CustomerRepository#findAllCustomer()
	 */
	@Override
	public List<Customer> findAllCustomer() {
		java.util.Map<Long, java.util.Map<String, Set<Object>>> map = concourse.select("firstName != null");
		List<Customer> list = new ArrayList<>();
		map.keySet().forEach(o -> {
			java.util.Map<String, Set<Object>> obj = map.get(o);
			Customer customer = new Customer();
			if(obj.get("id")!=null)
			customer.setId(obj.get("id").toString().replace("[","").replace("]","").trim());
			customer.setFirstName(obj.get("firstName").toString().replace("[","").replace("]","").trim());
			customer.setLastName(obj.get("lastName").toString().replace("[","").replace("]","").trim());
			list.add(customer);
		});
		return list;
	}

	/* (non-Javadoc)
	 * @see com.concourse.repository.CustomerRepository#deleteCustomer(java.lang.String)
	 */
	@Override
	public void deleteCustomer(String customerId) {
		try {
			java.util.Map<Long, java.util.Map<String, Set<Object>>> map = concourse.select("firstName != null");
			for(Long o:map.keySet()) {
				java.util.Map<String, Set<Object>> obj = map.get(o);
				
				if(obj.get("id")!=null && obj.get("id").toString().replace("[","").replace("]","").trim().equals(customerId)){
					//concourse.remove("id", customerId, obj);
					
				}
					
					
				
			}
            
        }
        finally {
            release(concourse);
        }

	}

	/* (non-Javadoc)
	 * @see com.concourse.repository.CustomerRepository#findCustomerById(java.lang.String)
	 */
	@Override
	public Customer findCustomerById(String customerId) {
		java.util.Map<Long, java.util.Map<String, Set<Object>>> map = concourse.select("firstName != null");
		Customer customer =  new Customer();
		map.keySet().forEach(o -> {
			java.util.Map<String, Set<Object>> obj = map.get(o);
			
			if(obj.get("id")!=null && obj.get("id").toString().toString().replace("[","").replace("]","").trim()==customerId){
				customer.setId(obj.get("id").toString());
				customer.setFirstName(obj.get("firstName").toString().replace("[","").replace("]","").trim());
				customer.setLastName(obj.get("lastName").toString().replace("[","").replace("]","").trim());
			}
				
				
			
		});
		return customer;
	}

}
