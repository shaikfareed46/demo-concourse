/**
*
*
*   
*
* Author : Shaik Fareed
*
* 
*
**/ 
package com.concourse.repository;

import java.util.List;

import com.concourse.entity.Customer;

public interface CustomerRepository {

    /**
     * @param firstName
     * @return
     */
    public Customer findByFirstName(String firstName);

	/**
	 * @param firstName
	 * @param lastName
	 * @return
	 */
	public Customer findByFirstNameAndLastName(String firstName, String lastName);

	/**
	 * @param customer
	 */
	public void saveCustomer(Customer customer);

	/**
	 * @return
	 */
	public List<Customer> findAllCustomer();

	/**
	 * @param customerId
	 */
	public void deleteCustomer(String customerId);

	/**
	 * @param customerId
	 * @return
	 */
	public Customer findCustomerById(String customerId);

}