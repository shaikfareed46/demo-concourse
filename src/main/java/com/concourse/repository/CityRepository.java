/**
*
*
*   
*
* Author : Shaik Fareed
*
* 
*
**/ 
package com.concourse.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.concourse.entity.City;

public interface CityRepository extends MongoRepository<City, String> {

}
