/**
 * 
 */


/**
 * @author sfareed
 *
 */
/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013-2016 Cinchapi Inc.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.concourse.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import com.cinchapi.concourse.ConnectionPool;
import com.google.common.base.Throwables;

/**
 * A collection of constants that are used throughout the project.
 * 
 * @author Jeff Nelson
 */
public class Constants {



    /**
     * A {@link ConnectionPool} that provides connections to Concourse on
     * demand. By default, this will connect to the server at
     * <em>localhost:1717</em>, but you can place a concourse_client.prefs file
     * with alternative connection information in the working directory or you
     * can use one of the other factory methods in the {@link ConnectionPool}
     * class to create the connection.
     */
    public static ConnectionPool CONCOURSE_CONNECTIONS = ConnectionPool
            .newCachedConnectionPool();

    /**
     * Singleton for the local MySQL connection.
     */

}
