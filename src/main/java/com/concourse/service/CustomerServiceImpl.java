/**
 * 
 * 
 * 
 * Author: Shaik Fareed
 * 
 * Name of project : Concourse Example 
*
 */
package com.concourse.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concourse.entity.Customer;
import com.concourse.exception.CustomerCreationException;
import com.concourse.exception.CustomerDeletionException;
import com.concourse.exception.CustomerLoadException;
import com.concourse.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepo;
	
	
	/* (non-Javadoc)
	 * @see com.concourse.service.CustomerService#saveCustomer(com.concourse.entity.Customer)
	 */
	@Override
	public void saveCustomer(Customer customer) throws CustomerCreationException {
		try{
			customerRepo.saveCustomer(customer);
		}
		catch(Exception ex){
			throw new CustomerCreationException(ex.getMessage(),ex);
		}
	}

	/* (non-Javadoc)
	 * @see com.concourse.service.CustomerService#findAllCustomer()
	 */
	@Override
	public List<Customer> findAllCustomer() throws CustomerLoadException {
		try{
			return customerRepo.findAllCustomer();
		}catch(Exception ex){
			throw new CustomerLoadException(ex.getMessage(),ex);
		}
		
	}

	/* (non-Javadoc)
	 * @see com.concourse.service.CustomerService#deleteCustomerById(java.lang.String)
	 */
	@Override
	public void deleteCustomerById(String customerId) throws CustomerDeletionException{
		try{
			customerRepo.deleteCustomer(customerId);
		}catch(Exception ex){
			throw new CustomerDeletionException(ex.getMessage(),ex);
		}
	}

	/* (non-Javadoc)
	 * @see com.concourse.service.CustomerService#findByFirstNameAndLastName(java.lang.String, java.lang.String)
	 */
	@Override
	public Customer findByFirstNameAndLastName(String firstName, String lastName) throws CustomerLoadException {
		try{
			return customerRepo.findByFirstNameAndLastName(firstName,lastName);
		}catch(Exception ex){
			throw new CustomerLoadException(ex.getMessage(),ex);
		}
	}

	/* (non-Javadoc)
	 * @see com.concourse.service.CustomerService#findCustomerById(java.lang.String)
	 */
	@Override
	public Customer findCustomerById(String customerId)  throws CustomerLoadException {
		try{
			return customerRepo.findCustomerById(customerId);
		}catch(Exception ex){
			throw new CustomerLoadException(ex.getMessage(),ex);
		}
	}


}
