/**
*
*
*   
*
* Author : Shaik Fareed
*
* 
*
**/ 
package com.concourse.service;

import java.util.List;

import com.concourse.entity.Customer;
import com.concourse.exception.CustomerCreationException;
import com.concourse.exception.CustomerDeletionException;
import com.concourse.exception.CustomerLoadException;

public interface CustomerService {

	/**
	 * @param customer
	 * @throws CustomerCreationException 
	 */
	void saveCustomer(Customer customer) throws CustomerCreationException;

	/**
	 * @return
	 * @throws CustomerLoadException 
	 */
	List<Customer> findAllCustomer() throws CustomerLoadException;

	/**
	 * @param customerId
	 * @throws CustomerDeletionException 
	 */
	void deleteCustomerById(String customerId) throws CustomerDeletionException;

	/**
	 * @param firstName
	 * @param lastName
	 * @return
	 * @throws CustomerLoadException 
	 */
	Customer findByFirstNameAndLastName(String firstName, String lastName) throws CustomerLoadException;

	/**
	 * @param customerId
	 * @return
	 * @throws CustomerLoadException 
	 */
	Customer findCustomerById(String customerId) throws CustomerLoadException;

	
}
