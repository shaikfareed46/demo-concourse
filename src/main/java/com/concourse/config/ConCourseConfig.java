/**
 * 
 */
package com.concourse.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.cinchapi.concourse.Concourse;
import com.concourse.repository.Constants;

/**
 * @author sfareed
 *
 */
@Configuration
public class ConCourseConfig {

	@Bean
	public Concourse concourse(){
		Concourse concourse =Concourse.connect();
		return concourse;
	}
	
}
